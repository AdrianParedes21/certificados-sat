import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-certificate',
  templateUrl: './certificate.component.html',
  styleUrls: ['./certificate.component.css']
})
export class CertificateComponent implements OnInit {
  form: FormGroup;
  base64textString;
  constructor() {
    this.form = new FormGroup({
      certificate: new FormControl('', Validators.required),
      base64: new FormControl('')
    });
  }

  ngOnInit() {
  }

  getCertificate() {
    const cert: string = this.form.get('certificate').value;
    const p1: string = cert.substr(0, 6);
    const p2: string = cert.substr(6, 6);
    const p3: string = cert.substr(12, 2);
    const p4: string = cert.substr(14, 2);
    const p5: string = cert.substr(16, 2);
    const url = 'https://rdc.sat.gob.mx/rccf/' + p1 + '/' + p2 + '/' + p3 + '/' + p4 + '/' + p5 + '/' + cert + '.cer';
    window.open(url);
    this.form.get('certificate').reset();
  }

  handleFileSelect(evt) {
    const files = evt.target.files;
    const file = files[0];

    if (files && file) {
      const reader = new FileReader();

      reader.onload = this._handleReaderLoaded.bind(this);

      reader.readAsBinaryString(file);
    }
  }

  _handleReaderLoaded(readerEvt) {
    const binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    this.form.get('base64').setValue(btoa(binaryString));
  }
}
